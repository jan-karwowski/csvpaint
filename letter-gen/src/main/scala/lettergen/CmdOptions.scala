package lettergen
import org.rogach.scallop.ScallopConf
import better.files.File

private[lettergen] final class CmdOptions(args: Seq[String])
    extends ScallopConf(args) {
  val file =
    opt[String](name = "output", short = 'o', required = true).map(x => File(x))
  val font = opt[String](name = "font", short = 'f', required = true)
  val width = opt[Int](name = "width", short = 'w', required = true)
  val height = opt[Int](name = "height", short = 'h', required = true)
  val letters = opt[String](
    name = "letters",
    short = 'l',
    default = Some("abcdefghijklmnopqrstuwvxyz")
  )

  verify()
}
