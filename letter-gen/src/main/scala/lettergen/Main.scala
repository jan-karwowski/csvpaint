package lettergen
import _root_.java.awt.image.BufferedImage
import java.awt.image.IndexColorModel
import java.awt.Font
import com.github.tototoshi.csv.CSVWriter

object Main {
  def main(args: Array[String]): Unit = {
    val options = new CmdOptions(args);

    options
      .file()
      .bufferedWriter
      .apply(bw => {
        val writer = new CSVWriter(bw)
        options
          .letters()
          .foreach(c => {
            writer.writeRow(
              genLetter(options.width(), options.height(), c, options.font())
            )
          })
        writer.close()
      })
  }

  def genLetter(
      w: Int,
      h: Int,
      letter: Char,
      fontFamily: String
  ): Vector[Int] = {
    val bitmapCM = new IndexColorModel(
      1,
      2,
      Array[Byte](0, -128),
      Array[Byte](0, -128),
      Array[Byte](0, -128)
    )
    val img = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_BINARY, bitmapCM)
    val graphics = img.createGraphics()
    val font = new Font(fontFamily, Font.PLAIN, h)
    graphics.setFont(font)
    val cw = graphics.getFontMetrics().charWidth(letter)
    val ch = graphics.getFontMetrics().getHeight()
    val cd = graphics.getFontMetrics().getDescent()
    graphics.drawChars(Array(letter), 0, 1, w / 2 - cw / 2, h - cd)
    Range(0, h)
      .flatMap(
        y =>
          Range(0, w).map(
            x =>
              if ((img.getRGB(x, y) & 0xFF) == 0) -1
              else 1
          )
      )
      .toVector
  }
}
