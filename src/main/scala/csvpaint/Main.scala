package csvpaint

import scala.collection.mutable.Stack
import scala.util.Try

import com.github.tototoshi.csv.{CSVReader, CSVWriter}
import javafx.scene.Node
import scalafx.application.JFXApp
import scalafx.beans.binding.Bindings
import scalafx.beans.property.{IntegerProperty, ObjectProperty}
import scalafx.scene.Scene
import scalafx.scene.control.{Button, ButtonType, Dialog, DialogPane, Label, Spinner, TextInputDialog, ToolBar}
import scalafx.scene.layout.{BorderPane, GridPane}
import scalafx.stage.FileChooser

final class ImageVector(size: Int)(
  previous : Stack[Vector[Boolean]] = Stack(),
  next : Stack[Vector[Boolean]] = Stack(),
  var current : Vector[Boolean] = Vector.fill(size)(false),
  val currentIdx : IntegerProperty = IntegerProperty(0)
) {
  require(current.length == size)
  previous.foreach(x => require(x.length == size))
  next.foreach(x => require(x.length == size))

  def this(initialSet : List[Vector[Boolean]]) = {
    this(initialSet.head.length)(Stack(), Stack( initialSet.tail : _*), initialSet.head)
  }

  def next(currentState : Vector[Boolean]) : Vector[Boolean] = {
    if(next.isEmpty) {
      current = currentState
    } else {
      previous.push(currentState)
      current = next.pop()
      currentIdx.value = currentIdx.value+1
    }
    current
  }

  def prev(currentState : Vector[Boolean]) : Vector[Boolean] = {
    if(previous.isEmpty) {
      current = currentState
    } else {
      next.push(currentState)
      current = previous.pop()
      currentIdx.value = currentIdx.value-1
    }
    current
  }

  def delete() : Vector[Boolean] = {
    if(previous.nonEmpty) {
      current = previous.pop()
      currentIdx.value = currentIdx.value - 1
    } else if(next.nonEmpty) {
      current = next.pop()
      currentIdx.value = currentIdx.value - 1
    } else {
      current = Vector.fill(size)(false)
    }
    current
  }

  def append(currentState : Vector[Boolean]) : Vector[Boolean] = {
    previous.push(currentState)
    currentIdx.value = currentIdx.value+1
    Vector.fill(size)(false)
  }

  def allVectors : List[Vector[Boolean]] = previous.reverse.toList ++ List(current) ++ next.toList
}

object Main extends JFXApp {
  val save = new Button("save")
  val load = new Button("load")
  val newSet = new Button("new")
  val next = new Button("next")
  val prev = new Button("prev")
  val delete = new Button("delete")
  val append = new Button("insert after")
  val toolbar = new ToolBar()
  val dialog = new FileChooser()
  val currentIdx = new Label("Current: 0")
  def currentBinding(prop: IntegerProperty) = Bindings.createStringBinding(() => s"Current ${prop.value}", prop)

  var currentVector = new ImageVector(100)()
  currentIdx.text <== currentBinding(currentVector.currentIdx)
  val currentCanvas = ObjectProperty[Canvas](new Canvas(10,10))

  toolbar.items.addAll(save, load, newSet, prev, next, append, delete, currentIdx)

  next.onAction = ae => {
    currentCanvas.value.setVector(currentVector.next(currentCanvas.value.vectorSnapshot))
  }

  prev.onAction = ae => {
    currentCanvas.value.setVector(currentVector.prev(currentCanvas.value.vectorSnapshot))
  }

  append.onAction = ae => {
    currentCanvas.value.setVector(currentVector.append(currentCanvas.value.vectorSnapshot))
  }

  delete.onAction = ae => {
    currentCanvas.value.setVector(currentVector.delete)
  }

  newSet.onAction = ae => {
    case class Dimensions(width: Int, height: Int)

    val dialog =new Dialog[Option[Dimensions]]() {
      initOwner(stage)
      title = "New image set"
      val w = new Spinner[Int](1, 100, 10)
      val h = new Spinner[Int](1, 100, 10)
      val grid = new GridPane() {
        add(new Label("width"), 0, 0)
        add(w, 1, 0)
        add(new Label("height"), 0, 1)
        add(h, 1, 1)
      }

      resultConverter = button => if(button == ButtonType.OK) {
        Some(Dimensions(w.value(), h.value()))
      } else {
        None
      }
      dialogPane().contentProperty().set(grid)
      new DialogPane(this.dialogPane()).buttonTypes = Seq(ButtonType.OK, ButtonType.Cancel)
    }

    val dim = dialog.showAndWait()
    dim match {
      case Some(Some(Dimensions(w,h))) => currentCanvas.value = new Canvas(w,h); currentVector = new ImageVector(w*h)();currentIdx.text <== currentBinding(currentVector.currentIdx)
      case _ => Unit
    }
  }

  save.onAction = ae => {
    currentVector.current = currentCanvas.value.vectorSnapshot
    val file = dialog.showSaveDialog(stage)
    val writer = CSVWriter.open(file)
    writer.writeAll(currentVector.allVectors.map(_.map(b => if(b) 1 else -1)))
    writer.close()
  }

  load.onAction = ae => {
    val file = dialog.showOpenDialog(stage)
    val reader = CSVReader.open(file)
    val data = reader.all().map(_.map(_.toInt).map(_==1).toVector)
    val size = data.head.length
    reader.close()
    val wDialog = new TextInputDialog(s"Input size: $size. Width?")
    def getWidth(w: Int=0) : Int = {
      if(w>0 && size%w == 0) w
      else {
        val ww = {
          val x = wDialog.showAndWait()
          Try(x.get.toInt).getOrElse(0)
        }
        getWidth(ww)
      }
    }

    val ww = getWidth()
    val hh = size/ww
    currentCanvas.value = new Canvas(ww, hh)
    currentVector = new ImageVector(data)
    currentIdx.text <== currentBinding(currentVector.currentIdx)
    currentCanvas.value.setVector(currentVector.current)
  }

  stage = new JFXApp.PrimaryStage {
    title = "Csv Paint"; width = 500; height = 1000
    scene = new Scene {
      root = new BorderPane {
        top = toolbar
        center <== Bindings.createObjectBinding[Node](() => currentCanvas.value, currentCanvas)
      }
    }
  }
}
