package csvpaint

import scalafx.Includes._
import scalafx.beans.property.BooleanProperty
import scalafx.geometry.Insets
import scalafx.scene.control.Label
import scalafx.scene.input.{MouseButton, MouseEvent}
import scalafx.scene.layout.{ Background, BackgroundFill, CornerRadii, GridPane, Priority }
import scalafx.scene.paint.Color

final class Canvas(width: Int, height: Int) extends GridPane {
  val vector = Vector.fill(width*height)(BooleanProperty(false))

  def pixelAt(x: Int, y: Int) = vector(x+y*width)

  val whiteBg = new Background(Array(new BackgroundFill(Color.White,new CornerRadii(0),Insets(0))))
  val blackBg = new Background(Array(new BackgroundFill(Color.Black,new CornerRadii(0),Insets(0))))

  this.vgrow = Priority.Always
  this.hgrow = Priority.Always

  def vectorSnapshot : Vector[Boolean] = vector.map(_.value)

  def setVector(vec: Vector[Boolean]) : Unit = {
    vector.zip(vec).foreach{case (prop, v) => prop.value = v}
  }

  private def mouseHandler(ae: MouseEvent) : Unit = {
    val posX = (ae.x*width/this.width()).toInt
    val posY = (ae.y*height/this.height()).toInt
    if(posX >=0 && posY >=0 && posX < width && posY < height) {
      if(ae.button == MouseButton.Primary) pixelAt(posX,posY).value = true
      else if(ae.button == MouseButton.Secondary) pixelAt(posX,posY).value = false
    }
  }


  this.onMousePressed = mouseHandler
  this.onMouseClicked = mouseHandler
  this.onMouseDragged = mouseHandler
  this.onMouseEntered = mouseHandler


  vector.zipWithIndex.foreach { case (v, idx) => 
    val pixel = Label(s"$idx")
    pixel.background <== when(v) choose whiteBg otherwise blackBg
    pixel.vgrow = Priority.Always
    pixel.hgrow = Priority.Always
    pixel.maxWidth = 10000
    pixel.maxHeight = 10000

    add(pixel, idx%width, idx/width)
  }
}
