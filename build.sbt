import java.io.File
enablePlugins(PackPlugin)
lazy val commonSettings = Seq(
  organization := "pl.edu.pw.mini.jk",
  version := "0.0-SNAPSHOT",
  scalaVersion := "2.12.9",
  //scalacOptions := Seq("-opt:l:classpath","-unchecked", "-feature", "-deprecation")
  scalacOptions := Seq("-unchecked", "-feature", "-deprecation"),
  javacOptions ++= Seq("-parameters"),
  packArchive := Seq(),
  libraryDependencies ++= Seq(
    "org.slf4s" %% "slf4s-api" % "1.7.25",
    "com.github.tototoshi" %% "scala-csv" % "1.3.5",
    "com.github.pathikrit" %% "better-files" % "3.6.0",
    "org.scalatest" %% "scalatest" % "3.0.1" % Test,
    "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test
  ),
  testOptions in Test += Tests.Argument("-oS")
)  
fork := true

packMain := Map(
  "csvpaint" -> "csvpaint.Main"
)

val jvmver = System.getProperty("java.specification.version")
val scalafx = if(jvmver == "1.8")
  Seq("org.scalafx" %% "scalafx" % "8.0.181-R13")
else if(jvmver == "11")
  Seq(
    "org.openjfx" % "javafx-base" % "13" classifier "linux",
    "org.openjfx" % "javafx-controls" % "13" classifier "linux",
    "org.openjfx" % "javafx-graphics" % "13" classifier "linux",
    "org.openjfx" % "javafx-swing" % "13" classifier "linux",
    "org.openjfx" % "javafx-media" % "13" classifier "linux",
    "org.scalafx" %% "scalafx" % "12.0.2-R18")
else
  throw new RuntimeException(s"Unsupported jvm version ${jvmver}")

packArchive := Seq(packArchiveTxz.value)


lazy val root = Project(id = "csvpaint", base = file(".")
).settings(commonSettings: _*).settings(Seq(
  libraryDependencies++=  scalafx 
))

lazy val letterGen = Project(id = "letter-gen", base = file("letter-gen"))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies += "org.rogach" %% "scallop" % "3.1.0",
    packMain := Map("lettergen" -> "lettergen.Main")
  )
  .enablePlugins(PackPlugin)

